package ru.varinskyv.photoreport.fragments;

import android.app.Activity;
import android.os.Bundle;
import android.preference.EditTextPreference;
import android.preference.Preference;
import android.preference.PreferenceFragment;
import android.preference.PreferenceScreen;
import android.support.design.widget.Snackbar;

import ru.varinskyv.photoreport.R;
import ru.varinskyv.photoreport.SettingsActivity;
import ru.varinskyv.photoreport.utils.FileHelper;
import ru.varinskyv.photoreport.utils.Preferences;

public class AppSettingsFragment extends PreferenceFragment implements Preference.OnPreferenceChangeListener, Preference.OnPreferenceClickListener {

    private EditTextPreference maxPhotoCount;
    private EditTextPreference photoDirectory;
    private EditTextPreference commentFileName;
    private PreferenceScreen ftpScreen;
    private PreferenceScreen emailScreen;
    private Activity activity;

    @Override
    public void onCreate(final Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.pref_app_settings);

        maxPhotoCount = (EditTextPreference) findPreference(Preferences.MAX_PHOTO_COUNT);
        maxPhotoCount.setOnPreferenceChangeListener(this);

        photoDirectory = (EditTextPreference) findPreference(Preferences.PHOTO_DIRECTORY);
        photoDirectory.setOnPreferenceChangeListener(this);

        commentFileName = (EditTextPreference) findPreference(Preferences.COMMENT_FILE_NAME);
        commentFileName.setOnPreferenceChangeListener(this);

        ftpScreen = (PreferenceScreen) findPreference(Preferences.FTP_SCREEN);
        ftpScreen.setOnPreferenceClickListener(this);

        emailScreen = (PreferenceScreen) findPreference(Preferences.EMAIL_SCREEN);
        emailScreen.setOnPreferenceClickListener(this);

        activity = getActivity();
    }

    @Override
    public void onResume() {
        super.onResume();

        activity.setTitle(R.string.settings_name);

        maxPhotoCount.setSummary(Preferences.getString(Preferences.MAX_PHOTO_COUNT));
        photoDirectory.setSummary(Preferences.getString(Preferences.PHOTO_DIRECTORY));
        commentFileName.setSummary(Preferences.getString(Preferences.COMMENT_FILE_NAME));

        if (FileHelper.getDirectoryPath() == null)
            FileHelper.initDirectory(Preferences.getString(Preferences.PHOTO_DIRECTORY));
    }

    @Override
    public boolean onPreferenceChange(Preference preference, Object newValue) {
        if (preference == photoDirectory) {
            if (FileHelper.changeDirectory(newValue.toString())) {
                FileHelper.initDirectory(newValue.toString());
                if (getView() != null)
                    Snackbar.make(getView(),
                            R.string.settings_photo_directory_changed_message,
                            Snackbar.LENGTH_LONG).show();
            }
            else {
                if (getView() != null)
                    Snackbar.make(getView(),
                            R.string.settings_photo_directory_changed_error_message,
                            Snackbar.LENGTH_LONG).show();

                return false;
            }
        }

        preference.setSummary(newValue.toString());

        return true;
    }

    @Override
    public boolean onPreferenceClick(Preference preference) {
        if (preference == ftpScreen){
            ((SettingsActivity)activity).changeFragment(new FTPSettingsFragment());
        }

        if (preference == emailScreen){
            ((SettingsActivity)activity).changeFragment(new EmailSettingsFragment());
        }

        return false;
    }
}
