package ru.varinskyv.photoreport.fragments;

import android.os.Bundle;
import android.preference.EditTextPreference;
import android.preference.Preference;
import android.preference.PreferenceFragment;
import android.preference.SwitchPreference;

import ru.varinskyv.photoreport.R;
import ru.varinskyv.photoreport.utils.Preferences;

public class FTPSettingsFragment extends PreferenceFragment implements Preference.OnPreferenceChangeListener {

    private SwitchPreference ftpMode;
    private EditTextPreference ftpAddress;
    private EditTextPreference ftpWorkDirectory;

    @Override
    public void onCreate(final Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.pref_ftp_settings);

        getActivity().setTitle(R.string.settings_ftp_category);

        ftpMode = (SwitchPreference) findPreference(Preferences.FTP_MODE);
        ftpMode.setOnPreferenceChangeListener(this);

        ftpAddress = (EditTextPreference) findPreference(Preferences.FTP_ADDRESS);
        ftpAddress.setOnPreferenceChangeListener(this);

        ftpWorkDirectory = (EditTextPreference) findPreference(Preferences.SERVER_WORK_DIRECTORY);
        ftpWorkDirectory.setOnPreferenceChangeListener(this);
    }

    @Override
    public void onResume() {
        super.onResume();

        setSummary();
    }

    @Override
    public boolean onPreferenceChange(Preference preference, Object newValue) {
        if (preference != ftpMode) {
            if (!newValue.equals("")) {
                preference.setSummary(newValue.toString());
            }
            else {
                if (preference == ftpAddress)
                    ftpAddress.setSummary(R.string.settings_ftp_address_summary);

                if (preference == ftpWorkDirectory)
                    ftpWorkDirectory.setSummary(R.string.settings_ftp_working_directory_summary);
            }
        }
        else
            if ((boolean) newValue)
                setSummary();

        return true;
    }

    private void setSummary() {
        if (!Preferences.getString(Preferences.FTP_ADDRESS).equals(""))
            ftpAddress.setSummary(Preferences.getString(Preferences.FTP_ADDRESS));

        if (!Preferences.getString(Preferences.SERVER_WORK_DIRECTORY).equals(""))
            ftpWorkDirectory.setSummary(Preferences.getString(Preferences.SERVER_WORK_DIRECTORY));
    }

}
