package ru.varinskyv.photoreport.fragments;

import android.app.Activity;
import android.os.Bundle;
import android.preference.EditTextPreference;
import android.preference.ListPreference;
import android.preference.Preference;
import android.preference.PreferenceFragment;
import android.preference.SwitchPreference;

import ru.varinskyv.photoreport.R;
import ru.varinskyv.photoreport.utils.Preferences;

public class EmailSettingsFragment extends PreferenceFragment implements Preference.OnPreferenceChangeListener {

    private SwitchPreference emailMode;
    private Activity activity;
    private EditTextPreference emailAddress;
    private EditTextPreference emailSubject;
    private ListPreference emailTransport;

    @Override
    public void onCreate(final Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.pref_email_settings);

        activity = getActivity();
        activity.setTitle(R.string.settings_email_category);

        emailMode = (SwitchPreference) findPreference(Preferences.EMAIL_MODE);
        emailMode.setOnPreferenceChangeListener(this);

        emailAddress = (EditTextPreference) findPreference(Preferences.EMAIL_ADDRESS);
        emailAddress.setOnPreferenceChangeListener(this);

        emailSubject = (EditTextPreference) findPreference(Preferences.EMAIL_SUBJECT);
        emailSubject.setOnPreferenceChangeListener(this);

        emailTransport = (ListPreference) findPreference(Preferences.EMAIL_TRANSPORT);
        emailTransport.setOnPreferenceChangeListener(this);
    }

    @Override
    public void onResume() {
        super.onResume();

        setSummary();
    }

    @Override
    public boolean onPreferenceChange(Preference preference, Object newValue) {
        if (preference != emailMode) {
            if (preference == emailTransport) {
                emailTransport.setSummary(activity.getResources()
                        .getStringArray(R.array.email_transport_names)
                        [Integer.valueOf(newValue.toString())]);
            }
            else {
                if (!newValue.equals("")) {
                    preference.setSummary(newValue.toString());
                }
                else {
                    if (preference == emailAddress)
                        emailAddress.setSummary(R.string.settings_ftp_address_summary);
                }
            }
        }
        else
            if((boolean) newValue)
                setSummary();

        return true;
    }

    private void setSummary() {
        if (!Preferences.getString(Preferences.EMAIL_ADDRESS).equals(""))
            emailAddress.setSummary(Preferences.getString(Preferences.EMAIL_ADDRESS));

        if (!Preferences.getString(Preferences.EMAIL_SUBJECT).equals(""))
            emailSubject.setSummary(Preferences.getString(Preferences.EMAIL_SUBJECT));

        emailTransport.setSummary(activity.getResources()
                .getStringArray(R.array.email_transport_names)
                [Integer.valueOf(Preferences.getString(Preferences.EMAIL_TRANSPORT))]);
    }

}
