package ru.varinskyv.photoreport;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import ru.varinskyv.photoreport.utils.BitmapHelper;
import ru.varinskyv.photoreport.utils.Constants;

public class ImageViewerActivity extends AppCompatActivity {

    public static final String TAG = "ImageViewerActivity";

    private ImageView image;

    private String photoFileName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image_viewer);

        if (getSupportActionBar() != null)
            getSupportActionBar().hide();

        toggleHideBar();

        image = (ImageView) findViewById(R.id.photo_image_image_viewer);

        Intent intent = getIntent();
        photoFileName = intent.getStringExtra(Constants.VIEW_PHOTO_FILE_NAME);

        DisplayMetrics metrics = getResources().getDisplayMetrics();
        int height = metrics.heightPixels;

        image.setImageBitmap(BitmapHelper.getBitmap(photoFileName, height, false));
    }

    @Override
    public void onBackPressed()
    {
        image.setImageBitmap(null);
        System.gc();

        finish();
        super.onBackPressed();
    }

    public void toggleHideBar() {

        int uiOptions = getWindow().getDecorView().getSystemUiVisibility();
        int newUiOptions = uiOptions;
        boolean isImmersiveModeEnabled =
                ((uiOptions | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY) == uiOptions);
        if (isImmersiveModeEnabled) {
            Log.d(TAG, "Turning immersive mode mode off. ");
        } else {
            Log.d(TAG, "Turning immersive mode mode on.");
        }

        if (Build.VERSION.SDK_INT >= 14) {
            newUiOptions ^= View.SYSTEM_UI_FLAG_HIDE_NAVIGATION;
        }

        if (Build.VERSION.SDK_INT >= 16) {
            newUiOptions ^= View.SYSTEM_UI_FLAG_FULLSCREEN;
        }

        if (Build.VERSION.SDK_INT >= 18) {
            newUiOptions ^= View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY;
        }

        getWindow().getDecorView().setSystemUiVisibility(newUiOptions);
    }
}
