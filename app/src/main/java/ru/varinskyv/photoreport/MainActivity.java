package ru.varinskyv.photoreport;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import ru.varinskyv.photoreport.utils.Constants;
import ru.varinskyv.photoreport.utils.FTPUploader;
import ru.varinskyv.photoreport.utils.FileHelper;
import ru.varinskyv.photoreport.utils.MailSender;
import ru.varinskyv.photoreport.utils.Preferences;

public class MainActivity extends AppCompatActivity implements View.OnClickListener, AdapterView.OnItemClickListener, FTPUploader.OnSendFile, AdapterView.OnItemLongClickListener, MailSender.OnSendEmail {

    public static final String TAG = "MainActivity";

    private RelativeLayout splash;

    private ArrayAdapter<String> ticketsAdapter;
    private ArrayList<String> ticketList = new ArrayList<>();
    private MenuItem sendAllToFTPMrnuItem;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_main);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab_main);
        fab.setOnClickListener(this);

        ticketsAdapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1,
                ticketList);

        ListView ticketListView;ticketListView = (ListView) findViewById(R.id.main_tickets_list);
        ticketListView.setOnItemClickListener(this);
        ticketListView.setOnItemLongClickListener(this);
        ticketListView.setAdapter(ticketsAdapter);

        splash = (RelativeLayout) findViewById(R.id.main_splash);
        splash.setOnClickListener(this);

        Preferences.init(this);
        loadTicketList(Preferences.getString(Preferences.TICKET_LIST));

        if (!Preferences.getString(Preferences.PHOTO_DIRECTORY).equals(""))
            FileHelper.initDirectory(Preferences.getString(Preferences.PHOTO_DIRECTORY));
    }

    @Override
    protected void onResume() {
        super.onResume();

        Preferences.init(this);
        if (sendAllToFTPMrnuItem != null)
            sendAllToFTPMrnuItem.setVisible(Preferences.getBoolean(Preferences.FTP_MODE));

        logMemory();

        if (Preferences.getString(Preferences.MAX_PHOTO_COUNT).equals("")) {
            Intent intent = new Intent(this, SettingsActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);

        sendAllToFTPMrnuItem = menu.findItem(R.id.menu_main_send);
        sendAllToFTPMrnuItem.setVisible(Preferences.getBoolean(Preferences.FTP_MODE));

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.menu_main_add) {
            startAddPhotoActivity(null);
            return true;
        }

        if (id == R.id.menu_main_send) {
            if (ticketList.size() > 0) {
                sendFTP(null);
            }
            else
                Toast.makeText(this, R.string.main_nothing_send_error_message, Toast.LENGTH_SHORT).show();
            return true;
        }

        if (id == R.id.menu_main_settings) {
            Intent intent = new Intent(this, SettingsActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();

        if(id == R.id.fab_main)
            startAddPhotoActivity(null);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        startAddPhotoActivity(ticketList.get(position));
    }

    @Override
    public boolean onItemLongClick(AdapterView<?> parent, View view, final int position, long id) {
        final ArrayList<String> dialogItems = getDialogItems();

        new AlertDialog.Builder(this)
                .setInverseBackgroundForced(true)
                .setTitle(R.string.main_edit_ticket_dialog_title)
                .setAdapter(new ArrayAdapter<String>(this,
                                android.R.layout.select_dialog_item, dialogItems),
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialogResult(dialogItems, which, position);
                            }
                        })
                .create().show();

        return true;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent intent) {
        if (resultCode == RESULT_OK) {
            if (intent != null) {
                if (Preferences.getString(Preferences.EDIT_TICKET_NAME).equals(""))
                    addTicket(intent.getStringExtra(Constants.TICKET));
                else
                    renameTicket(intent.getStringExtra(Constants.TICKET));
            }
        }
    }

    @Override
    public void onSendDone(boolean result) {
        splash.setVisibility(View.GONE);
        ticketsAdapter.notifyDataSetChanged();
        saveTicketList();

        if (result)
            Toast.makeText(this, R.string.main_send_done_message, Toast.LENGTH_SHORT).show();
        else
            Toast.makeText(this, R.string.main_send_error_message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onSendFile(boolean result, String ticket) {
        if (result) {
            int maxPhotoCount = Integer.valueOf(Preferences.getString(Preferences.MAX_PHOTO_COUNT));
            for (int i = 0; i < maxPhotoCount; i++) {
                String photoFile = FileHelper.getPhotoFileName(ticket, i);
                if (FileHelper.existFile(photoFile))
                    FileHelper.getPhotoFile(photoFile).delete();
            }

            Preferences.remove(Preferences.COMMENT + ticket);

            ticketList.remove(ticketList.indexOf(ticket));
        }
    }

    @Override
    public void onSendEmail(boolean result) {
        splash.setVisibility(View.GONE);
    }

    private void loadTicketList(String tickets) {
        JSONArray jsonArray = new JSONArray();

        try {
            jsonArray = new JSONArray(tickets);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        ticketList.clear();
        for (int i = 0; i < jsonArray.length(); i++) {
            JSONObject jsonObject = jsonArray.optJSONObject(i);
            ticketList.add(jsonObject.optString(Constants.TICKET));
        }

        ticketsAdapter.notifyDataSetChanged();
    }

    private void startAddPhotoActivity(String ticket) {
        Intent intent = new Intent(this, AddPhotoActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);

        if (ticket != null)
            intent.putExtra(Constants.TICKET, ticket);

        startActivityForResult(intent, 0);
    }

    private void deleteTicket(int index)
    {
        int maxPhotoCount = Integer.valueOf(Preferences.getString(Preferences.MAX_PHOTO_COUNT));
        for (int i = 0; i < maxPhotoCount; i++) {
            FileHelper.deleteFile(FileHelper.getPhotoFileName(ticketList.get(index), i));
        }

        ticketList.remove(index);
        ticketsAdapter.notifyDataSetChanged();

        saveTicketList();
    }

    private void addTicket(String ticket) {
        if (ticketList.indexOf(ticket) >= 0)
            ticketList.remove(ticketList.indexOf(ticket));

        ticketList.add(ticket);
        ticketsAdapter.notifyDataSetChanged();

        saveTicketList();

        System.gc();
        logMemory();
    }

    private void renameTicket(String newTicketName) {
        if (ticketList.indexOf(Preferences.getString(Preferences.EDIT_TICKET_NAME)) >= 0)
            ticketList.remove(ticketList.indexOf(Preferences.getString(
                    Preferences.EDIT_TICKET_NAME)));

        Preferences.remove(Preferences.EDIT_TICKET_NAME);

        addTicket(newTicketName);
    }

    private void saveTicketList() {
        JSONArray jsonArray = new JSONArray();

        for (String ticket: ticketList) {
            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put(Constants.TICKET, ticket);
                jsonArray.put(jsonObject);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        Preferences.putString(Preferences.TICKET_LIST, jsonArray.toString());
    }

    private void logMemory() {
        Log.d(TAG, String.format(getString(R.string.log_memory_message),
                (int) (Runtime.getRuntime().totalMemory() / 1024)));
    }

    private ArrayList<String> getDialogItems() {
        ArrayList<String> result = new ArrayList<>();

        if (Preferences.getBoolean(Preferences.EMAIL_MODE))
            result.add(getResources().getString(R.string.main_dialog_send_by_email));

        if (Preferences.getBoolean(Preferences.FTP_MODE))
            result.add(getResources().getString(R.string.main_dialog_send_to_ftp));

        result.add(getResources().getString(R.string.main_dialog_rename));
        result.add(getResources().getString(R.string.main_dialog_delete));

        return result;
    }

    private void dialogResult(ArrayList<String> dialogItems, int which, int position) {
        if (dialogItems.get(which).equals(getResources()
                .getString(R.string.main_dialog_send_by_email))) {
            sendEmail(position);
        }

        if (dialogItems.get(which).equals(getResources()
                .getString(R.string.main_dialog_send_to_ftp))) {
            sendFTP(position);
        }

        if (dialogItems.get(which).equals(getResources()
                .getString(R.string.main_dialog_rename))) {
            startAddPhotoActivity(ticketList.get(position));
        }

        if (dialogItems.get(which).equals(getResources()
                .getString(R.string.main_dialog_delete))) {
            deleteTicket(position);
        }
    }

    private void sendFTP(Integer position) {
        splash.setVisibility(View.VISIBLE);

        if (position == null)
            new FTPUploader(ticketList, this).execute();
        else
            new FTPUploader(ticketList.get(position), this).execute();
    }

    private void sendEmail(Integer position) {
        splash.setVisibility(View.VISIBLE);

        if (position == null)
            new MailSender(this, ticketList, this).execute();
        else
            new MailSender(this, ticketList.get(position), this).execute();
    }

}
