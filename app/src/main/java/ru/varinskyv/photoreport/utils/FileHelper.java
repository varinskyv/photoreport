package ru.varinskyv.photoreport.utils;

import android.net.Uri;
import android.os.Environment;
import android.util.Log;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;

public class FileHelper {

    public static final String TAG = "FileHelper";

    private static String directoryPath;

    public static void initDirectory(String directoryName) {
        File directory = new File(Environment.getExternalStorageDirectory().getAbsolutePath(),
                directoryName);
        if (!directory.exists())
            directory.mkdirs();

        directoryPath = directory.getAbsolutePath();

        Log.d(TAG, String.format("directoryPath = %s", directoryPath));
    }

    public static String getDirectoryPath() {
        return directoryPath;
    }

    public static String getDirectoryName() {
        if (getDirectoryPath() == null)
            return "";

        return new File(getDirectoryPath()).getName();
    }

    public static String getPhotoFileName(String ticket, int index) {
        return ticket + "_" + String.valueOf(index) + ".jpg";
    }

    public static String getAbsoluteFile(String fileName) {
        return getDirectoryPath() + "/" + fileName;
    }

    public static File getPhotoFile(String fileName) {
        return new File(getAbsoluteFile(fileName));
    }

    public static Uri getPhotoFileUri(String ticket, int index) {
        return Uri.fromFile(new File(getAbsoluteFile(getPhotoFileName(ticket, index))));
    }

    public static boolean existFile(String fileName) {
        File file = new File(getAbsoluteFile(fileName));
        return file.exists();
    }

    public static boolean deleteFile(String fileName) {
        File file = new File(getAbsoluteFile(fileName));
        return file.delete();
    }

    public static void renameFile(String oldFileName, String newFileName) {
        File oldFile = new File(getAbsoluteFile(oldFileName));
        File newFile = new File(getAbsoluteFile(newFileName));

        oldFile.renameTo(newFile);
    }

    public static boolean changeDirectory(String newDirectoryName) {
        if (getDirectoryPath() == null || newDirectoryName.equals(""))
            return false;

        File oldDirectory = new File(getDirectoryPath());
        File newDirectory = new File(Environment.getExternalStorageDirectory().getAbsolutePath(),
                newDirectoryName);

        return oldDirectory.renameTo(newDirectory);
    }

    public static File createCommentFile(String ticket) {
        File result = new File(getAbsoluteFile(
                Preferences.getString(Preferences.COMMENT_FILE_NAME)));

        try {
            BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(result));
            bufferedWriter.write(Preferences.getString(Preferences.COMMENT + ticket));
            bufferedWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return result;
    }

    public static boolean copyFile(String fileName, String newFileName) {
        try {
            FileInputStream in = new FileInputStream(new File(getAbsoluteFile(fileName)));
            FileOutputStream out = new FileOutputStream(new File(getAbsoluteFile(newFileName)));

            byte[] buffer = new byte[1024];
            int read;
            while ((read = in.read(buffer)) != -1) {
                out.write(buffer, 0, read);
            }
            in.close();
            out.flush();
            out.close();

            return true;
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return false;
    }

    public static boolean copyFileToWorkDirectory(String absoluteFileName, String newFileName) {
        try {
            FileInputStream in = new FileInputStream(new File(absoluteFileName));
            FileOutputStream out = new FileOutputStream(new File(getAbsoluteFile(newFileName)));

            byte[] buffer = new byte[1024];
            int read;
            while ((read = in.read(buffer)) != -1) {
                out.write(buffer, 0, read);
            }
            in.close();
            out.flush();
            out.close();

            return true;
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return false;
    }

}
