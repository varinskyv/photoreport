package ru.varinskyv.photoreport.utils;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.util.Log;

public class BitmapHelper {

    public static final String TAG = "BitmapHelper";



    public static Bitmap getBitmap(String photoFileName, int maxSize, boolean reverseRotate) {
        boolean isNeedRotate = false;

        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(photoFileName, options);

        if (options.outWidth > options.outHeight) {
            options.inSampleSize = calculateInSampleSize(options.outWidth, maxSize);
            if (reverseRotate)
                isNeedRotate = true;
        }
        else
        {
            options.inSampleSize = calculateInSampleSize(options.outHeight, maxSize);
            if (!reverseRotate)
                isNeedRotate = true;
        }

        options.inPreferredConfig = Bitmap.Config.RGB_565;
        options.inJustDecodeBounds = false;

        Bitmap bitmap = BitmapFactory.decodeFile(photoFileName, options);

        Matrix matrix = new Matrix();
        if (isNeedRotate)
            matrix.postRotate(90);

        Bitmap result = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(),
                bitmap.getHeight(), matrix, true);

        Log.d(TAG, String.format("Total memory = %s Kb",
                (int) (Runtime.getRuntime().totalMemory() / 1024)));

        System.gc();

        return result;
    }

    public static Bitmap getBitmap(String photoFile) {
        return getBitmap(photoFile, Constants.PHOTO_MAX_SIZE, false);
    }

    public static int calculateInSampleSize(int maxPhotoSize, int neededMaxSize) {
        int inSampleSize = 1;

        if (maxPhotoSize > neededMaxSize) {
            final int halfMaxSize = maxPhotoSize / 2;

            while ((halfMaxSize / inSampleSize) > neededMaxSize) {
                inSampleSize *= 2;
            }
        }

        return inSampleSize;
    }

}
