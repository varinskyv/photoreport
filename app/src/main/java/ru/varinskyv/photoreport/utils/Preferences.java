package ru.varinskyv.photoreport.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

public class Preferences {

    public static SharedPreferences preferences;

    public static final String PHOTO_DIRECTORY = "photo_directory";
    public static final String COMMENT_FILE_NAME = "comment_file_name";

    public static final String TICKET_LIST = "ticket_list";

    public static final String MAX_PHOTO_COUNT = "max_photo_count";

    public static final String FTP_SCREEN = "ftp_screen";
    public static final String FTP_MODE = "ftp_mode";
    public static final String FTP_ADDRESS = "ftp_address";
    public static final String SERVER_WORK_DIRECTORY = "server_work_directory";
    public static final String SERVER_USER = "server_user";
    public static final String SERVER_PASSWORD = "server_password";

    public static final String EMAIL_SCREEN = "email_screen";
    public static final String EMAIL_MODE = "email_mode";
    public static final String EMAIL_ADDRESS = "email_address";
    public static final String EMAIL_SUBJECT = "email_subject";
    public static final String EMAIL_TRANSPORT = "email_transport";

    public static final String COMMENT = "comment_";
    public static final String EDIT_TICKET_NAME = "edit_ticket_name";

    public static void init(Context context) {
        Preferences.preferences = PreferenceManager.getDefaultSharedPreferences(context);
    }


    public static boolean remove(String key) {
        return preferences.edit().remove(key).commit();
    }

    public static void putString(String key, String string) {
        preferences.edit().putString(key, string).commit();
    }

    public static String getString(String key) {
        return preferences.getString(key, "");
    }

    public static void putInt(String key, int value) {
        preferences.edit().putInt(key, value).commit();
    }

    public static int getInt(String key) {
        return preferences.getInt(key, 0);
    }

    public static void putBoolean(String key, boolean value) {
        preferences.edit().putBoolean(key, value).commit();
    }

    public static boolean getBoolean(String key) {
        return preferences.getBoolean(key, false);
    }

}
