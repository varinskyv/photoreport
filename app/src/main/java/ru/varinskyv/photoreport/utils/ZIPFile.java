package ru.varinskyv.photoreport.utils;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

public class ZIPFile {

    private static final int BUFFER = 1024;

    public static boolean zip(File zipFileName, String[] files) {
        boolean result = false;

        ZipOutputStream zos = null;
        try {
            zos = new ZipOutputStream(new BufferedOutputStream(new FileOutputStream(zipFileName)));

            for (int i = 0; i < files.length; i++) {
                File srcFile = new File(files[i]);

                ZipEntry entry = new ZipEntry(srcFile.getName());
                zos.putNextEntry(entry);

                byte[] buf = new byte[BUFFER];
                int len;
                FileInputStream in = new FileInputStream(srcFile);
                while ((len = in.read(buf)) > 0) {
                    zos.write(buf, 0, len);
                }

                zos.closeEntry();

                result = true;
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (zos != null) {
                    zos.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        return result;
    }

}
