package ru.varinskyv.photoreport.utils;

import android.os.AsyncTask;

import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.InetAddress;
import java.util.ArrayList;

import ru.varinskyv.photoreport.MainActivity;

public class FTPUploader extends AsyncTask<Void, String, Boolean> {

    private ArrayList<String> ticketList = null;
    private String report = null;
    private OnSendFile onSendFile;
    FTPClient ftpClient = new FTPClient();
    private String ftpServer;
    private String user;
    private String password;
    private String workingDirectory;

    public interface OnSendFile {
        public void onSendDone(boolean result);
        public void onSendFile(boolean result, String ticket);
    }

    public FTPUploader(ArrayList<String> ticketList, OnSendFile onSendFile) {
        this.ticketList = new ArrayList<String>(ticketList);
        this.onSendFile = onSendFile;
    }

    public FTPUploader(String report, MainActivity onSendFile) {
        this.report = report;
        this.onSendFile = onSendFile;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();

        ftpServer = Preferences.getString(Preferences.FTP_ADDRESS);
        workingDirectory = Preferences.getString(Preferences.SERVER_WORK_DIRECTORY);;
        user = Preferences.getString(Preferences.SERVER_USER);;
        password = Preferences.getString(Preferences.SERVER_PASSWORD);;
    }

    @Override
    protected Boolean doInBackground(Void... params) {
        boolean result = false;

        if (ticketList == null && report == null)
            return result;

        if (login()) {
            changeWorkingDirectory(workingDirectory);

            if (ticketList == null) {
                ticketList = new ArrayList<>();
                ticketList.add(report);
            }

            for (int i = 0; i < ticketList.size(); i++) {
                String ticket = ticketList.get(i);

                File commentFile = FileHelper.createCommentFile(ticket);
                File tempZipFile  = new File(FileHelper.getAbsoluteFile("temp.zip"));

                ArrayList<String> fileList = new ArrayList<>();
                fileList.add(commentFile.getAbsolutePath());

                int maxPhotoCount = Integer.valueOf(Preferences.getString(
                        Preferences.MAX_PHOTO_COUNT));
                for (int j = 0; j < maxPhotoCount; j++) {
                    String photoFile = FileHelper.getPhotoFileName(ticket, j);
                    if (FileHelper.existFile(photoFile))
                        fileList.add(FileHelper.getAbsoluteFile(photoFile));
                }

                result = ZIPFile.zip(tempZipFile, fileList.toArray(new String[fileList.size()]));

                if (result)
                    result = sendFile(ticket + ".zip", tempZipFile, FTP.BINARY_FILE_TYPE);

                commentFile.delete();
                tempZipFile.delete();

                if (result) {
                    if (onSendFile != null)
                        onSendFile.onSendFile(result, ticket);
                }
            }

            logout();
        }

        return result;
    }

    private boolean login() {
        try {
            ftpClient.connect(ftpServer);//InetAddress.getByName(ftpServer));
            return ftpClient.login(user, password);
        } catch (IOException e) {
            if (e != null)
                e.printStackTrace();
        }

        return false;
    }

    private boolean logout() {
        try {
            ftpClient.logout();
            ftpClient.disconnect();
            return true;
        } catch (IOException e) {
            e.printStackTrace();
        }

        return false;
    }

    private boolean changeWorkingDirectory(String directory) {
        try {
            return ftpClient.changeWorkingDirectory(directory);
        } catch (IOException e) {
            e.printStackTrace();
        }

        return false;
    }

    private boolean makeDirectory(String directory) {
        try {
            ftpClient.makeDirectory(directory);
            return ftpClient.changeWorkingDirectory(directory);
        } catch (IOException e) {
            e.printStackTrace();
        }

        return false;
    }

    private boolean sendFile(String fileName, File file, int fileType) {
        try {
            ftpClient.enterLocalPassiveMode();
            ftpClient.setFileType(fileType);

            BufferedInputStream buffIn = new BufferedInputStream(new FileInputStream(file));
            ftpClient.storeFile(fileName, buffIn);
            buffIn.close();

            return true;
        } catch (IOException e) {
            e.printStackTrace();
        }

        return false;
    }

    @Override
    protected void onPostExecute(Boolean result) {
        super.onPostExecute(result);
         if (onSendFile != null)
            onSendFile.onSendDone(result);
    }
}
