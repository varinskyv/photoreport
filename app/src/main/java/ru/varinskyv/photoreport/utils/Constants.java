package ru.varinskyv.photoreport.utils;

public class Constants {

    public static final String TICKET = "TICKET";

    public static final String TEMP_PHOTO_FILE_TEMPLATE = "temp";
    public static final String VIEW_PHOTO_FILE_NAME = "VIEW_PHOTO_FILE_NAME";

    public static final int PHOTO_MAX_SIZE = 100;

    public static final int PICK_FROM_CAMERA = 0;
    public static final int PICK_FROM_GALLERY = 1;

}
