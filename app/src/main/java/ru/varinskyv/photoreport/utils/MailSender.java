package ru.varinskyv.photoreport.utils;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;

import java.io.File;
import java.util.ArrayList;

public class MailSender extends AsyncTask<Void, Void, Boolean> {

    private Context context;
    private ArrayList<String> ticketList = null;
    private OnSendEmail onSendEmail;
    private String ticket = null;
    private String email;
    private String subject;
    private int emailTransport;
    private String emailBody;

    public interface OnSendEmail {
        public void onSendEmail(boolean result);
    }

    public MailSender(Context context, ArrayList<String> ticketList, OnSendEmail onSendEmail) {
        this.context = context;
        this.ticketList = ticketList;
        this.onSendEmail = onSendEmail;
    }

    public MailSender(Context context, String ticket, OnSendEmail onSendEmail) {
        this.context = context;
        this.ticket = ticket;
        this.onSendEmail = onSendEmail;
    }

    @Override
    protected Boolean doInBackground(Void... params) {
        boolean result = false;

        if (ticket == null && ticketList == null)
            return result;

        email = Preferences.getString(Preferences.EMAIL_ADDRESS);
        subject = Preferences.getString(Preferences.EMAIL_SUBJECT);

        emailTransport = Integer.valueOf(Preferences.getString(Preferences.EMAIL_TRANSPORT));

        if (emailTransport == 0) {
            if (ticket == null)
                return result;

            emailBody = Preferences.getString(Preferences.COMMENT + ticket);

            Intent emailIntent = new Intent(Intent.ACTION_SEND_MULTIPLE);
            emailIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            emailIntent.setType("plain/text");//plain/text//message/rfc822

            email = email.replace("\\s", "");
            emailIntent.putExtra(Intent.EXTRA_EMAIL  , email.split(","));
            emailIntent.putExtra(Intent.EXTRA_SUBJECT, subject);
            emailIntent.putExtra(Intent.EXTRA_TEXT, emailBody);

            ArrayList<Uri> uri = new ArrayList<>();
            int maxPhotoCount = Integer.valueOf(Preferences.getString(Preferences.MAX_PHOTO_COUNT));
            for (int j = 0; j < maxPhotoCount; j++) {
                String photoFile = FileHelper.getPhotoFileName(ticket, j);
                if (FileHelper.existFile(photoFile))
                    //fileList.add(FileHelper.getAbsoluteFile(photoFile));
                //File file = new File(attachmentPath.get(i));
                uri.add(Uri.fromFile(FileHelper.getPhotoFile(photoFile)));
            }

            emailIntent.putParcelableArrayListExtra(Intent.EXTRA_STREAM, uri);

            try {
                context.startActivity(Intent.createChooser(emailIntent, "Отправка письма..."));

                result = true;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        else {
//            Session session = createSessionObject();
//
//            try {
//                MimeMessage message = createMessage(responses, subject, emailBody, session);
//
//                Transport.send(message);
//
//                result = true;
//            } catch (MessagingException e) {
//                e.printStackTrace();
//            } catch (UnsupportedEncodingException e) {
//                e.printStackTrace();
//            }
        }

        return result;
    }

//    private Session createSessionObject() {
//        Properties properties = new Properties();
//
//        String[] transportName = context.getResources().getStringArray(R.array.email_transport);
//
//        properties.put("mail.smtp.host", transportName[emailTransport]);
//        properties.put("mail.smtp.port", "465");
//        properties.put("mail.smtp.auth", "true");
//        properties.put("mail.smtp.socketFactory.port", "465");
//        properties.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
//        properties.put("mail.smtp.socketFactory.fallback", "false");
//        properties.put("mail.smtp.starttls.enable", "true");
//
//        return Session.getDefaultInstance(properties, new Authenticator() {
//
//            @Override
//            protected PasswordAuthentication getPasswordAuthentication() {
//                return new PasswordAuthentication(username, password);
//            }
//
//        });
//    }
//
//    private MimeMessage createMessage(String recipients, String subject, String messageBody, Session session) throws MessagingException, UnsupportedEncodingException {
//        MimeMessage message = new MimeMessage(session);
//        message.setFrom(new InternetAddress(username, senderName));
//
//        if (recipients.indexOf(",") > 0)
//            message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(recipients));
//        else
//            message.setRecipient(Message.RecipientType.TO, new InternetAddress(recipients));
//
//        message.setSubject(subject);
//        message.setText(messageBody);
//        return message;
//    }

    @Override
    protected void onPostExecute(Boolean result) {
        if (onSendEmail != null)
            onSendEmail.onSendEmail(result);
    }

}
