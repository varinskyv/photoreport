package ru.varinskyv.photoreport;

import android.app.Fragment;
import android.os.Bundle;
import android.preference.PreferenceFragment;

import ru.varinskyv.photoreport.fragments.AppSettingsFragment;
import ru.varinskyv.photoreport.fragments.EmailSettingsFragment;
import ru.varinskyv.photoreport.fragments.FTPSettingsFragment;

public class SettingsActivity extends AppCompatPreferenceActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getFragmentManager().beginTransaction()
                .replace(android.R.id.content, new AppSettingsFragment()).commit();
    }

    protected boolean isValidFragment(String fragmentName) {
        return PreferenceFragment.class.getName().equals(fragmentName)
                || AppSettingsFragment.class.getName().equals(fragmentName)
                || FTPSettingsFragment.class.getName().equals(fragmentName)
                || EmailSettingsFragment.class.getName().equals(fragmentName);
    }

    public void changeFragment(Fragment fragment) {
        getFragmentManager().beginTransaction()
                .replace(android.R.id.content, fragment).addToBackStack("").commit();
    }

}
