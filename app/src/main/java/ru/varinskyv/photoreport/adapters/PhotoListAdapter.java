package ru.varinskyv.photoreport.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import ru.varinskyv.photoreport.R;
import ru.varinskyv.photoreport.utils.BitmapHelper;
import ru.varinskyv.photoreport.utils.FileHelper;

public class PhotoListAdapter extends ArrayAdapter<String> {

    private final Context context;
    private final List<String> list;
    private ViewHolder viewHolder;

    private class ViewHolder
    {
        private ImageView itemPhoto;
        private TextView itemText;
    }

    public PhotoListAdapter(Context context, List<String> list) {
        super(context, R.layout.item_photo_list, list);

        this.context = context;
        this.list = list;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
        View photoListView = convertView;
        if (photoListView == null)
        {
            LayoutInflater inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            photoListView = inflater.inflate(R.layout.item_photo_list, parent, false);

            viewHolder = new ViewHolder();

            viewHolder.itemPhoto = (ImageView) photoListView.findViewById(R.id.photo_list_item_image);
            viewHolder.itemText = (TextView) photoListView.findViewById(R.id.photo_list_item_text);

            photoListView.setTag(viewHolder);
        }
        else
            viewHolder = (ViewHolder)photoListView.getTag();

        String photoFile = list.get(position);

        viewHolder.itemPhoto.setImageBitmap(null);
        viewHolder.itemPhoto.setImageBitmap(BitmapHelper
                .getBitmap(FileHelper.getAbsoluteFile(photoFile)));

        viewHolder.itemText.setText(photoFile);

        return photoListView;
    }

}
