package ru.varinskyv.photoreport;

import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import ru.varinskyv.photoreport.adapters.PhotoListAdapter;
import ru.varinskyv.photoreport.utils.Constants;
import ru.varinskyv.photoreport.utils.FileHelper;
import ru.varinskyv.photoreport.utils.Preferences;

public class AddPhotoActivity extends AppCompatActivity implements View.OnClickListener, AdapterView.OnItemClickListener {

    public static final String TAG = "AddPhotoActivity";

    private RelativeLayout rootLayout;
    private ImageButton editButton;
    private EditText ticketNumber;
    private EditText comment;
    private ListView photoListView;
    private FloatingActionButton fab;
    private MenuItem menuItemCamera;
    private MenuItem menuItemSave;

    private PhotoListAdapter photoAdapter;
    private ArrayList<String> photoList = new ArrayList<>();
    private boolean isMenuItemCameraVisible = true;
    private boolean isEditMode;
    private boolean isEditTicket;
    private int addPhotoAction;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_photo);

        Preferences.init(this);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_add_photo);
        setSupportActionBar(toolbar);

        rootLayout = (RelativeLayout) findViewById(R.id.add_photo_root);

        editButton = (ImageButton) findViewById(R.id.add_photo_edit_ticket_button);
        editButton.setOnClickListener(this);

        ticketNumber = (EditText) findViewById(R.id.add_photo_ticket_name);

        comment = (EditText) findViewById(R.id.add_photo_comment);

        photoAdapter = new PhotoListAdapter(this, photoList);

        photoListView = (ListView) findViewById(R.id.add_photo_photo_list);
        photoListView.setOnItemClickListener(this);
        photoListView.setAdapter(photoAdapter);

        fab = (FloatingActionButton) findViewById(R.id.fab_add_photo);
        fab.setOnClickListener(this);

        isEditMode = false;
        isEditTicket = false;

        Intent intent = getIntent();
        loadTicketData(intent.getStringExtra(Constants.TICKET));

        logMemory();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        //outState.putStringArrayList("photoList", photoList);
        //outState.putBoolean("isMenuItemCameraVisible", isMenuItemCameraVisible);
        //outState.putBoolean("isEditMode", isEditMode);
        //outState.putBoolean("isEditTicket", isEditTicket);
        //outState.putInt("addPhotoAction", addPhotoAction);
        //outState.putString("editedTicket", editedTicket);

        Log.d(TAG, "onSaveInstanceState");
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);

        //photoList = savedInstanceState.getStringArrayList("photoList");
        //isMenuItemCameraVisible = savedInstanceState.getBoolean("isMenuItemCameraVisible");
        //isEditMode = savedInstanceState.getBoolean("isEditMode");
        //isEditTicket = savedInstanceState.getBoolean("isEditTicket");
        //addPhotoAction = savedInstanceState.getInt("addPhotoAction");
        //editedTicket = savedInstanceState.getString("editedTicket");

        Log.d(TAG, "onRestoreInstanceState");
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_add_photo, menu);

        menuItemCamera = menu.findItem(R.id.menu_add_photo_camera);
        menuItemSave = menu.findItem(R.id.menu_add_photo_save);
        setMenuItemCameraVisible(isMenuItemCameraVisible);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.menu_add_photo_camera) {
            startAddPhotoChooser(null);
            return true;
        }

        if (id == R.id.menu_add_photo_save) {
            saveChanges();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(final View v) {
        int id = v.getId();

        if (id == R.id.fab_add_photo) {
            startAddPhotoChooser(null);
        }

        if (id == R.id.add_photo_edit_ticket_button) {
            if (isEditTicket) {
                if (!ticketNumber.getText().toString().equals(ticketNumber.getTag())) {
                    for (int i = 0; i < photoList.size(); i++) {
                        FileHelper.renameFile(photoList.get(i),
                                FileHelper.getPhotoFileName(ticketNumber.getText().toString(), i));
                    }

                    addPhotosToList(ticketNumber.getText().toString());
                }

                restoreViewsAfterTicketEdit();
            }
            else {
                editButton.setImageResource(android.R.drawable.ic_menu_save);

                int color = Color.parseColor("#80000000");
                rootLayout.setBackgroundColor(color);

                comment.setEnabled(false);
                photoListView.setEnabled(false);
                fab.setVisibility(View.GONE);
                menuItemCamera.setVisible(false);
                menuItemSave.setVisible(false);

                ticketNumber.setEnabled(true);
                ticketNumber.setTag(ticketNumber.getText().toString());
                ticketNumber.requestFocus();

                isEditTicket = true;
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);

        if (resultCode == RESULT_OK) {
            switch (addPhotoAction) {

                case Constants.PICK_FROM_CAMERA:
                    updatePhotoList(requestCode);
                    break;

                case Constants.PICK_FROM_GALLERY:
                    copyFromGallery(requestCode, intent);
                    break;

            }
        }
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, final int position, long id) {
        new AlertDialog.Builder(this)
                .setInverseBackgroundForced(true)
                .setTitle(R.string.add_photo_edit_photo_dialog_title)
                .setMessage(R.string.add_photo_edit_photo_dialog_message)
                .setIcon(android.R.drawable.ic_dialog_info)
                .setPositiveButton(R.string.main_edit_ticket_dialog_positive_button_text,
                        new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        deletePhoto(position);
                    }
                })
                .setNegativeButton(R.string.add_photo_edit_photo_dialog_negative_button_text,
                        new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        startAddPhotoChooser(position);
                    }
                })
                .setNeutralButton(R.string.add_photo_edit_photo_dialog_neutral_button_text,
                        new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        viewPhoto(position);
                    }
                })
                .create().show();
    }

    @Override
    public void onBackPressed()
    {
        if (isEditTicket) {
            ticketNumber.setText((String) ticketNumber.getTag());
            restoreViewsAfterTicketEdit();

            return;
        }
        else {
            for (int i = 0; i < photoList.size(); i++) {
                FileHelper.deleteFile(photoList.get(i));
            }
            int maxPhotoCount = Integer.valueOf(Preferences.getString(Preferences.MAX_PHOTO_COUNT));
            for (int i = 0; i < maxPhotoCount; i++) {
                String photoFile = FileHelper.getPhotoFileName(
                        Preferences.getString(Preferences.EDIT_TICKET_NAME), i);
                String tempPhotoFile = FileHelper.getPhotoFileName(
                        Constants.TEMP_PHOTO_FILE_TEMPLATE, i);
                if (FileHelper.existFile(tempPhotoFile))
                    FileHelper.renameFile(tempPhotoFile, photoFile);
            }

            Preferences.remove(Preferences.EDIT_TICKET_NAME);

            finish();
        }

        super.onBackPressed();
    }

    private void loadTicketData(String ticket) {
        if (ticket == null)
            return;

        if (!ticket.equals("")) {
            isEditMode = true;

            Preferences.putString(Preferences.EDIT_TICKET_NAME, ticket);

            ticketNumber.setText(ticket);
            comment.setText(Preferences.getString(Preferences.COMMENT + ticket));

            addPhotosToList(ticket);
        }
    }

    private void startAddPhotoChooser(final Integer position) {
        if (!ticketNumber.getText().toString().equals("")) {
            if (!ticketExist(ticketNumber.getText().toString())) {
                final ArrayList<String> dialogItems = new ArrayList<>();
                dialogItems.add(getResources().getString(R.string.add_photo_dialog_camera));
                dialogItems.add(getResources().getString(R.string.add_photo_dialog_gallery));

                new AlertDialog.Builder(this)
                        .setInverseBackgroundForced(true)
                        .setTitle(R.string.main_edit_ticket_dialog_title)
                        .setAdapter(new ArrayAdapter<String>(this,
                                        android.R.layout.select_dialog_item, dialogItems),
                                new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        dialogResult(dialogItems, which, position);
                                    }
                                })
                        .create().show();
            }
            else
                Toast.makeText(this, R.string.add_photo_ticket_exist_error_message,
                        Toast.LENGTH_SHORT).show();
        }
        else
            //Snackbar.make(get,
            //        R.string.settings_photo_directory_changed_message,
            //        Snackbar.LENGTH_LONG).show();
            Toast.makeText(this, R.string.add_photo_empty_ticket_number_error_message,
                    Toast.LENGTH_SHORT).show();
    }

    private void dialogResult(ArrayList<String> dialogItems, int which, Integer position) {
        if (dialogItems.get(which).equals(getResources()
                .getString(R.string.add_photo_dialog_camera))) {
            if (position == null)
                startCamera(photoList.size());
            else
                startCamera(position);
        }

        if (dialogItems.get(which).equals(getResources()
                .getString(R.string.add_photo_dialog_gallery))) {
            if (position == null)
                openGallery(photoList.size());
            else
                openGallery(position);
        }
    }

    private void startCamera(int photoIndex)
    {
        addPhotoAction = Constants.PICK_FROM_CAMERA;

        System.gc();

        Uri uri = FileHelper.getPhotoFileUri(ticketNumber.getText().toString(), photoIndex);

        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, uri);

        try {
            if (intent.resolveActivity(getPackageManager()) != null)
                startActivityForResult(intent, photoIndex);
            else
                Toast.makeText(this, R.string.add_photo_start_camera_error_message,
                        Toast.LENGTH_LONG).show();
        } catch (Exception e) {
            e.printStackTrace();

            Toast.makeText(this, R.string.add_photo_start_camera_error_message,
                    Toast.LENGTH_LONG).show();
        }
    }

    public void openGallery(int photoIndex) {
        addPhotoAction = Constants.PICK_FROM_GALLERY;

        System.gc();

        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setType("image/*");
        intent.putExtra("return-data", true);

        try {
            if (intent.resolveActivity(getPackageManager()) != null)
                startActivityForResult(Intent.createChooser(intent, "Выбор приложения"), photoIndex);
            else
                Toast.makeText(this, R.string.add_photo_start_camera_error_message,
                        Toast.LENGTH_LONG).show();
        } catch (Exception e) {
            e.printStackTrace();

            Toast.makeText(this, R.string.add_photo_start_camera_error_message,
                    Toast.LENGTH_LONG).show();
        }
    }

    private void saveChanges() {
        if (!ticketNumber.getText().toString().equals("")) {
            if (!ticketExist(ticketNumber.getText().toString())) {
                Intent intent = new Intent();
                intent.putExtra(Constants.TICKET, ticketNumber.getText().toString());
                setResult(RESULT_OK, intent);

                Preferences.putString(Preferences.COMMENT + ticketNumber.getText().toString(),
                        comment.getText().toString());

                int maxPhotoCount = Integer.valueOf(Preferences.getString(Preferences.MAX_PHOTO_COUNT));
                for (int i = 0; i < maxPhotoCount; i++) {
                    String tempPhotoFile = FileHelper.getPhotoFileName(
                            Constants.TEMP_PHOTO_FILE_TEMPLATE, i);
                    if (FileHelper.existFile(tempPhotoFile))
                        FileHelper.deleteFile(tempPhotoFile);
                }

                finish();
            }
            else
                Toast.makeText(this, R.string.add_photo_ticket_exist_error_message,
                        Toast.LENGTH_SHORT).show();
        }
        else
            Toast.makeText(this, R.string.add_photo_empty_ticket_number_error_message,
                    Toast.LENGTH_SHORT).show();
    }

    private boolean ticketExist(String ticket) {
        if (isEditMode)
            return false;

        JSONArray jsonArray = new JSONArray();

        try {
            jsonArray = new JSONArray(Preferences.getString(Preferences.TICKET_LIST));
        } catch (JSONException e) {
            e.printStackTrace();
            return false;
        }

        for (int i = 0; i < jsonArray.length(); i++) {
            JSONObject jsonObject = jsonArray.optJSONObject(i);
            if (ticket.equals(jsonObject.optString(Constants.TICKET)))
                return true;
        }

        return false;
    }

    private void updatePhotoList(int index) {
        String photoFile = FileHelper.getPhotoFileName(ticketNumber.getText().toString(), index);

        if (photoList.indexOf(photoFile) >= 0)
            photoList.remove(index);

        photoList.add(index, photoFile);
        photoAdapter.notifyDataSetChanged();

        updateViews();

        System.gc();
        logMemory();
    }

    private void copyFromGallery(int index, Intent intent) {
        if (intent != null) {
            try {
                Uri selectedImage = intent.getData();

                String[] filePathColumn = { MediaStore.Images.Media.DATA };

                Cursor cursor = getContentResolver().query(selectedImage, filePathColumn,
                        null, null, null);
                cursor.moveToFirst();

                int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                String filePath = cursor.getString(columnIndex);

                Log.d("Attachment Path:", filePath);

                String photoFile = FileHelper.getPhotoFileName(ticketNumber.getText().toString(),
                        index);

                if (FileHelper.copyFileToWorkDirectory(Uri.parse("file://" + filePath).getPath(),
                        photoFile))
                    updatePhotoList(index);

                cursor.close();
            }
            catch (Exception e) {
                e.printStackTrace();

                Toast.makeText(this, R.string.add_photo_start_gallery_error_message,
                        Toast.LENGTH_SHORT).show();
            }
        }
    }

    private void updateViews() {
        int maxPhotoCount = Integer.valueOf(Preferences.getString(Preferences.MAX_PHOTO_COUNT));
        if (photoList.size() == maxPhotoCount) {
            fab.setVisibility(View.GONE);
            setMenuItemCameraVisible(false);
        }
        else {
            fab.setVisibility(View.VISIBLE);
            setMenuItemCameraVisible(true);
        }

        if (photoList.size() > 0) {
            ticketNumber.setEnabled(false);
            editButton.setVisibility(View.VISIBLE);
        }
        else {
            ticketNumber.setEnabled(true);
            editButton.setVisibility(View.GONE);
        }
    }

    private void setMenuItemCameraVisible(boolean visible) {
        if (menuItemCamera != null)
            menuItemCamera.setVisible(visible);
        else
            if (isMenuItemCameraVisible != visible)
                isMenuItemCameraVisible = visible;
    }

    private void deletePhoto(int position) {
        if (FileHelper.deleteFile(photoList.get(position))) {
            photoList.remove(position);

            for (int i = position; i < photoList.size(); i++) {
                FileHelper.renameFile(photoList.get(i),
                        FileHelper.getPhotoFileName(ticketNumber.getText().toString(), i));
            }

            addPhotosToList(ticketNumber.getText().toString());
        }
    }

    private void addPhotosToList(String ticket) {
        photoList.clear();

        int maxPhotoCount = Integer.valueOf(Preferences.getString(Preferences.MAX_PHOTO_COUNT));
        for (int i = 0; i < maxPhotoCount; i++) {
            String photoFile = FileHelper.getPhotoFileName(ticket, i);
            if (FileHelper.existFile(photoFile)) {
                photoList.add(photoFile);

                String tempPhotoFile = FileHelper.getPhotoFileName(
                        Constants.TEMP_PHOTO_FILE_TEMPLATE, i);
                if (isEditMode && !FileHelper.existFile(tempPhotoFile)) {
                    FileHelper.copyFile(photoFile, tempPhotoFile);
                }
            }
        }

        photoAdapter.notifyDataSetChanged();

        updateViews();
    }

    private void viewPhoto(int position) {
        System.gc();

        Intent intent = new Intent(this, ImageViewerActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);

        String photoFile = FileHelper.getPhotoFileName(ticketNumber.getText().toString(), position);
        intent.putExtra(Constants.VIEW_PHOTO_FILE_NAME, FileHelper.getAbsoluteFile(photoFile));

        startActivity(intent);
    }

    private void restoreViewsAfterTicketEdit() {
        editButton.setImageResource(android.R.drawable.ic_menu_edit);

        int color = Color.parseColor("#ffffff");
        rootLayout.setBackgroundColor(color);

        comment.setEnabled(true);
        photoListView.setEnabled(true);
        menuItemSave.setVisible(true);

        updateViews();

        isEditTicket = false;
    }

    private void logMemory() {
        Log.d(TAG, String.format(getString(R.string.log_memory_message),
                (int) (Runtime.getRuntime().totalMemory() / 1024)));
    }

}
